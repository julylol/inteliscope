USE inteliscope;

CREATE TABLE user (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  email                    VARCHAR(30) NOT NULL, 
  password                 VARCHAR(100) NOT NULL, 
  name               	   VARCHAR(30) NOT NULL, 
  product             	   VARCHAR(30) NOT NULL,
  shooter_type             VARCHAR(100) NOT NULL,
  auth_token 			   VARCHAR(191) UNIQUE NOT NULL,
  registration_token 	   VARCHAR(191) UNIQUE NOT NULL,
  is_enabled               BOOL NOT NULL DEFAULT FALSE,
  PRIMARY KEY (id)
)
  CHARACTER SET utf8mb4
  COLLATE utf8mb4_unicode_ci;

CREATE TABLE video (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  filename varchar(191) NOT NULL,
  status varchar(191) NOT NULL,
  user_id bigint(20) NOT NULL,
  fileurl varchar(191) DEFAULT NULL,
  title varchar(191) NOT NULL,
  description varchar(191) DEFAULT NULL,
  tags varchar(191) DEFAULT NULL,
  height varchar(191) NOT NULL,
  width varchar(191) NOT NULL,
  category varchar(191) DEFAULT NULL,
  ageRestriction tinyint(1) DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES user (id)
    ON DELETE CASCADE
) 
  CHARACTER SET utf8mb4
  COLLATE utf8mb4_unicode_ci;