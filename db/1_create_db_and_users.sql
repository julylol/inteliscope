CREATE DATABASE IF NOT EXISTS inteliscope DEFAULT CHARACTER SET utf8mb4;

CREATE USER 'inteli'@'%' IDENTIFIED BY 'scope';
GRANT INSERT,SELECT,UPDATE,DELETE ON inteliscope.* TO 'inteli'@'%';

USE inteliscope;
