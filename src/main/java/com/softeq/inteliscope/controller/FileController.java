package com.softeq.inteliscope.controller;

import org.springframework.stereotype.Controller;

import java.io.File;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/file")
@Controller
public class FileController {

    @Path("/{fileToken}/download")
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getFile(@PathParam("fileToken") String fileToken) {
        //get file path by token
        File file = new File("C:\\Users\\Public\\Pictures\\Sample Pictures\\Koala.jpg");
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM)
                .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"") //optional
                .build();
    }

}
