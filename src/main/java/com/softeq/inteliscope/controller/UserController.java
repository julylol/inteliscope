package com.softeq.inteliscope.controller;

import com.softeq.inteliscope.entity.request.RegisterRequest;
import com.softeq.inteliscope.entity.response.RegisterResponse;
import com.softeq.inteliscope.service.UserService;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/user")
public class UserController {

    @Inject
    private UserService userService;

    @Path("/register")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public RegisterResponse register(@Valid RegisterRequest registerRequest) {
        return userService.register(registerRequest);
    }


}
