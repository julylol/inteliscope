package com.softeq.inteliscope.exceptionhandler;

import com.softeq.inteliscope.exception.CheckedInternalServerErrorException;

import org.jboss.logging.Logger;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class CheckedInternalServerErrorExceptionHandler implements ExceptionMapper<CheckedInternalServerErrorException> {
    private static final Logger LOGGER = Logger.getLogger(CheckedInternalServerErrorExceptionHandler.class);

    @Override
    public Response toResponse(CheckedInternalServerErrorException e) {
        LOGGER.error(e.getMessage(), e);
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
}
