package com.softeq.inteliscope.schedule;

import com.softeq.inteliscope.dao.VideoDao;
import com.softeq.inteliscope.entity.Video;
import com.softeq.inteliscope.entity.VideoStatus;
import com.softeq.inteliscope.singleton.VideoMerge;

import org.jboss.logging.Logger;

import java.util.List;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class FailedVideoSchedule {

	private static final Logger LOGGER = Logger.getLogger(FailedVideoSchedule.class);

	@Inject
	private VideoDao videoDao;

	@Schedule(hour="*", persistent = true)
	public void mergeVideo() {
		LOGGER.debug("Failed video merger start");

		VideoMerge videoMerge = VideoMerge.getInstance();

		synchronized (videoMerge) {
			List<Video> videos = videoDao
					.getVideoByStatus(VideoStatus.MERGEFAILED.getStatus());

			for (Video video : videos) {
				videoMerge.runVideoMerge(video);
			}

		}
	}

}
