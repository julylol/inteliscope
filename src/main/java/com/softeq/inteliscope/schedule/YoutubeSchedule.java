package com.softeq.inteliscope.schedule;

import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;

import com.softeq.inteliscope.config.FilePathConfig;
import com.softeq.inteliscope.config.YoutubeConfig;
import com.softeq.inteliscope.dao.VideoDao;
import com.softeq.inteliscope.entity.Video;
import com.softeq.inteliscope.entity.VideoStatus;
import com.softeq.inteliscope.singleton.YoutubeUpload;

import org.jboss.logging.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class YoutubeSchedule {

	private static final String APPLICATION_NAME = "RIFLESCOPE";
	private static final String MSG_NOTIFICATION_FAILED = APPLICATION_NAME
			+ " : " + "Failed to notify person (%s) about the video #%s";
	private static final String MSG_NULL_SMTP_USER = APPLICATION_NAME + " : "
			+ "SMTP user is not specified, email notification is disabled";
	private static final String MSG_UPLOAD_SUCCESS = APPLICATION_NAME + " : "
			+ "YouTube video upload completed successfully, video #%s (%s)";
	private static final String MSG_UPLOAD_FAILED = APPLICATION_NAME + " : "
			+ "YouTube video upload failed, video #%s (%s)";
	private static final String MSG_RUN_FAILED = APPLICATION_NAME + " : "
			+ "YouTube video upload service failed to execute";
	private static final String MSG_START_SERVICE = APPLICATION_NAME + " : "
			+ "Starting YouTube upload, video #%s (%s)";
	private static final String MSG_FILE_DELETE_FAILED = APPLICATION_NAME
			+ " : " + "FAILED TO DELETE REDUNDANT VIDEO FILE: %s";

	private static final String YOUTUBE_BASE_URL = "https://www.youtube.com/watch?v=";

	private static final Logger LOGGER = Logger.getLogger(YoutubeSchedule.class);

	@Inject
	private VideoDao videoDao;

	@Schedule(hour="*", persistent = true)
	public void uploadVideo() {
		try {
			LOGGER.info("Video upload start");

			YoutubeUpload uploadService = YoutubeUpload.getInstance();

			synchronized (uploadService) {

				List<Video> videos = videoDao.getVideoByStatus(VideoStatus.MERGED
						.getStatus());

				YouTube service = getYouTubeService();

				for (Video video : videos) {

					try {
						LOGGER.info(String.format(MSG_START_SERVICE, video.getId(),
								video.getTitle()));

					com.google.api.services.youtube.model.Video returnedVideo = uploadService
							.uploadVideo(video, service);

						if (null != returnedVideo) {

							// update video record, delete file on disc, send email notification

							video.setUrl(YOUTUBE_BASE_URL + returnedVideo.getId());
							videoDao.setCompleted(video);
							removeMergedFile(video);
							sendConfirmationEmail(video);

							LOGGER.info(String.format(MSG_UPLOAD_SUCCESS,
									video.getId(), video.getTitle()));
						}

					} catch (Exception e) {
						LOGGER.error(String.format(MSG_UPLOAD_FAILED,
								video.getId(), video.getTitle()));
						LOGGER.error(e);
					}
				}
			}

		} catch (Exception e) {
			LOGGER.error(MSG_RUN_FAILED);
			LOGGER.error(e);
		}
	}

	/**
	 * send youtube upload confirmation
	 */
	private void sendConfirmationEmail(Video video) {
		//TODO send email
	}

	/**
	 * initiate YouTube service with appropriate credentials
	 * @throws IOException
	 */
	private YouTube getYouTubeService() throws IOException {
		final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
		final JsonFactory JSON_FACTORY = new JacksonFactory();

		String refreshToken = getRefreshToken();

		GoogleCredential credential = createCredentialWithRefreshToken(
				HTTP_TRANSPORT, JSON_FACTORY,
				new TokenResponse().setRefreshToken(refreshToken));

		credential.refreshToken();

		// YouTube object used to make all API requests.
		YouTube youtube = new YouTube.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				credential).setApplicationName(APPLICATION_NAME).build();

		return youtube;
	}

	/**
	 * retrieve refresh token by the property path specified
	 * @throws IOException
	 */
	private String getRefreshToken() throws IOException {
			String tokenLocation = YoutubeConfig.getYoutubeRefreshToken();
			try(BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(tokenLocation).getAbsoluteFile())))
			{
				return bufferedReader.readLine();
			}
	}

	/**
	 * get an access token using the existing refresh token.
	 * @throws IOException
	 */
	public GoogleCredential createCredentialWithRefreshToken(
			HttpTransport transport, JsonFactory jsonFactory,
			TokenResponse tokenResponse) throws IOException {
		GoogleCredential credential = new GoogleCredential.Builder()
				.setTransport(transport).setJsonFactory(jsonFactory)
				.setClientSecrets(loadClientSecrets()).build()
				.setFromTokenResponse(tokenResponse);

		return credential;
	}

	/**
	 * load client ID/Secret from file.
	 * 
	 * @throws IOException
	 */
	private GoogleClientSecrets loadClientSecrets() throws IOException {
		GoogleClientSecrets clientSecrets = GoogleClientSecrets
				.load(new JacksonFactory(), new InputStreamReader(
						getClientSecrets()));

		return clientSecrets;
	}

	/**
	 * retrieve refresh token by the property path specified
	 * @throws FileNotFoundException 
	 */
	private InputStream getClientSecrets() throws FileNotFoundException {
		String clientSecretsLocation = YoutubeConfig.getYoutubeClientSecrets();
		return new FileInputStream(clientSecretsLocation);
	}

	/**
	 * get merged with AD file path
	 */
	private String getMergedFilePath(Video video) {
		return FilePathConfig.getPathMerge() + video.getFilename();
	}

	/**
	 * remove merged with AD file
	 */
	private void removeMergedFile(Video video) {
		//TODO remove file
	}

}
