package com.softeq.inteliscope.dao;

import com.softeq.inteliscope.entity.Video;

import java.util.List;


public interface VideoDao {
	
	int createVideo(Video video);
	Video getVideoById(Integer id);
    List<Video> getVideoByStatus(String status);
    void updateStatus(List<Video> video, String status);
    void updateStatus(Video video, String status);
    
    void setMerged(Video video, boolean mergeStatus);
    void setCompleted(Video video);

}
