package com.softeq.inteliscope.dao;

import com.softeq.inteliscope.entity.User;

public interface UserDao {

	void insert(User user);
	User find(String email);
	
}
