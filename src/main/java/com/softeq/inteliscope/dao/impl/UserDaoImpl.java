package com.softeq.inteliscope.dao.impl;

import com.softeq.inteliscope.dao.UserDao;
import com.softeq.inteliscope.entity.User;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

@Stateless
public class UserDaoImpl implements UserDao {

	private JdbcTemplate jdbcTemplate;
	
	private static final String INSERT_USER_SQL = "INSERT INTO user " +
			"(email, password, name, product, shooter_type, auth_token, is_enabled) VALUES (?, ?, ?, ?, ?, ?, ?)";
	
	private static final String SELECT_USER_SQL = "SELECT * FROM user WHERE email = ?";

	JdbcTemplate getJdbcTemplate() {
		if (null == jdbcTemplate) {
			try {
				InitialContext context = new InitialContext();
				jdbcTemplate = new JdbcTemplate(
						(DataSource) context.lookup("java:/inteliscope"));
			} catch (NamingException e) {

			}
		}
		return jdbcTemplate;
	}

	private static final class FindUserMapper implements RowMapper<Object>{
		@Override
		public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
			User user = new User();
			user.setEmail(rs.getString(1));
			user.setPassword(rs.getString(2));
			user.setName(rs.getString(3));
			user.setProduct(rs.getString(4));
			user.setShooterType(rs.getString(5));
			user.setAuthToken(rs.getString(6));
			user.setIsEnabled(rs.getBoolean(7));
			return user;
		}		
	}
	
	@Override
	public void insert(User user) {
		getJdbcTemplate().update(INSERT_USER_SQL, new Object[]{user.getEmail(),
				user.getPassword(), user.getName(), user.getProduct(), user.getShooterType(), user.getAuthToken(), user.isEnabled()
		});		
	}

	@Override
	public User find(String email) {
		User user = (User) getJdbcTemplate().queryForObject(SELECT_USER_SQL, new Object[]{email}, new FindUserMapper());
		return user;
	}

}












