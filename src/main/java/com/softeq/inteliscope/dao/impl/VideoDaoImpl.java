package com.softeq.inteliscope.dao.impl;

import com.softeq.inteliscope.dao.VideoDao;
import com.softeq.inteliscope.entity.Video;
import com.softeq.inteliscope.entity.VideoStatus;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

@Stateless
public class VideoDaoImpl implements VideoDao {

	JdbcTemplate jdbcTemplate;
	
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	private static final String SQL_ONE_VIDEOS = "SELECT * FROM video WHERE id=?";
	private static final String SQL_SAVE_VIDEOS = "INSERT INTO video (filename,status,email,url,title,description,tags,height,width,category,ageRestriction,mergeDetails) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String SQL_STATUS_VIDEO = "SELECT * FROM video WHERE status=?";
	private static final String SQL_STATUS_UPDATE_ALL = "UPDATE video SET status=:status WHERE id IN (:id)";
	private static final String SQL_MERGE = "UPDATE video SET status=:status, mergeDetails=:mergeDetails WHERE id=:id";
	private static final String SQL_STATUS_UPDATE_ONE = "UPDATE video SET status=:status WHERE id=:id";
	private static final String SQL_STATUS_UPDATE_COMPLETED = "UPDATE video SET status=:status, url=:url WHERE id=:id";
	
		
	JdbcTemplate getJdbcTemplate() {
		if (null == jdbcTemplate) {
			try {
				InitialContext context = new InitialContext();
				jdbcTemplate = new JdbcTemplate((DataSource) context.lookup("java:/inteliscope"));
			} catch (NamingException e) {
				
			}
		}
		return jdbcTemplate;
	}
	
	NamedParameterJdbcTemplate getNamedParameterJdbcTemplate(){
		if (null == namedParameterJdbcTemplate) {
			try {
				InitialContext context = new InitialContext();
				namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(
						(DataSource) context.lookup("java:/riflescope"));
			} catch (NamingException e) {
				
			}
		}
		return namedParameterJdbcTemplate;
	}


	private static final class VideosMapperId implements RowMapper<Object> {
		@Override
		public Object mapRow(ResultSet resultSet, int i) throws SQLException {
			Video video = new Video();
			video.setId(resultSet.getInt(1));
			video.setFilename(resultSet.getString(2));
			video.setStatus(resultSet.getString(3));
			video.setEmail(resultSet.getString(4));
			video.setUrl(resultSet.getString(5));
			video.setTitle(resultSet.getString(6));
			video.setDescription(resultSet.getString(7));
			video.setTags(resultSet.getString(8));
			video.setHeight(resultSet.getString(9));
			video.setWidth(resultSet.getString(10));
			video.setCategory(resultSet.getString(11));
			video.setAgeRestriction(resultSet.getInt(12) == 1);
			return video;
		}
	}	

	private static final class VideoMapperStatus implements RowMapper<Video>{

		@Override
		public Video mapRow(ResultSet resultSet, int rowNum) throws SQLException {
			Video video = new Video();
			video.setId(resultSet.getInt(1));
			video.setFilename(resultSet.getString(2));
			video.setStatus(resultSet.getString(3));
			video.setEmail(resultSet.getString(4));
			video.setUrl(resultSet.getString(5));
			video.setTitle(resultSet.getString(6));
			video.setDescription(resultSet.getString(7));
			video.setTags(resultSet.getString(8));
			video.setHeight(resultSet.getString(9));
			video.setWidth(resultSet.getString(10));
			video.setCategory(resultSet.getString(11));
			video.setAgeRestriction(resultSet.getInt(12) == 1);
			return video;
		}
		
	}
	
	@Override
	public int createVideo(final Video video) {
		
		KeyHolder holder = new GeneratedKeyHolder();

		getJdbcTemplate().update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(
						SQL_SAVE_VIDEOS, Statement.RETURN_GENERATED_KEYS);
				
				ps.setString(1, video.getFilename());
				ps.setString(2, video.getStatus());
				ps.setString(3, video.getEmail());
				ps.setString(4, video.getUrl());
				ps.setString(5, video.getTitle());
				ps.setString(6, video.getDescription());
				ps.setString(7, video.getTags());
				ps.setString(8, video.getHeight());
				ps.setString(9, video.getWidth());
				ps.setString(10, video.getCategory());
				ps.setBoolean(11, video.getAgeRestriction());
				ps.setString(12, video.getMergeDetails());			
				return ps;
			}
		}, holder);

		return holder.getKey().intValue();

	}
	
	@Override
	public Video getVideoById(Integer id) {
		Video video = (Video) getJdbcTemplate().queryForObject(SQL_ONE_VIDEOS,
				new Object[] {id}, new VideosMapperId());
		return video;
	}
	
	@Override
	public List<Video> getVideoByStatus(String status) {		
		return  getJdbcTemplate().query(SQL_STATUS_VIDEO, new Object[] {status}, new VideoMapperStatus());
	}

	@Override
	public void updateStatus(List<Video> videos, String status) { 
		Set<Integer> id = new HashSet<Integer>();
		for (Video video : videos){		
			id.add(video.getId());		
		}				
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("status", status);
		parameterSource.addValue("id", id);
		System.out.println(parameterSource);		
		getNamedParameterJdbcTemplate().update(SQL_STATUS_UPDATE_ALL, parameterSource);
	}

	@Override
	public void updateStatus(Video video, String status) {
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("status", status);
		mapSqlParameterSource.addValue("id", video.getId());				
		getNamedParameterJdbcTemplate().update(SQL_STATUS_UPDATE_ONE, mapSqlParameterSource);			
	}

	@Override
	public void setMerged(Video video, boolean mergeStatus) {
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();		
		if (mergeStatus){
		mapSqlParameterSource.addValue("status", VideoStatus.MERGED.getStatus());
		} else {
		mapSqlParameterSource.addValue("status", VideoStatus.MERGEFAILED.getStatus());
		}
		mapSqlParameterSource.addValue("mergeDetails", video.getMergeDetails());
		mapSqlParameterSource.addValue("id", video.getId());
		getNamedParameterJdbcTemplate().update(SQL_MERGE, mapSqlParameterSource);	
	}

	@Override
	public void setCompleted(Video video) {
		MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
		mapSqlParameterSource.addValue("status", VideoStatus.COMPLETED.getStatus());
		mapSqlParameterSource.addValue("url", video.getUrl());
		mapSqlParameterSource.addValue("id", video.getId());
		getNamedParameterJdbcTemplate().update(SQL_STATUS_UPDATE_COMPLETED, mapSqlParameterSource);			
	}
	
	
}














