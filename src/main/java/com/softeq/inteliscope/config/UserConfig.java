package com.softeq.inteliscope.config;

import org.jboss.logging.Logger;

public class UserConfig {
    private static final Logger LOGGER = Logger.getLogger(UserConfig.class);

    private static final String KEY_INTELISCOPE_USER_AUTH_TOKEN_SALT = "inteliscope_user_auth_token_salt";
    private static final String KEY_INTELISCOPE_CONFIRM_TOKEN_LENGTH = "inteliscope_user_confirm_token_length";

    private static final int CONFIRM_TOKEN_LENGTH = 10;

    public static String getUserAuthTokenSalt() {
        return System.getProperty(KEY_INTELISCOPE_USER_AUTH_TOKEN_SALT);
    }

    public static int getConfirmTokenLength() {
        return tryParseInt(System.getProperty(KEY_INTELISCOPE_CONFIRM_TOKEN_LENGTH), CONFIRM_TOKEN_LENGTH);
    }

    private static int tryParseInt(String s, int defaultValue) {
        int result = defaultValue;
        if (s == null) {
            return result;
        }

        try {
            result = Integer.parseInt(s);
        } catch (NumberFormatException ex) {
            LOGGER.warn(String.format("error with parsing '%s' to int", s));
        }
        return result;
    }
}
