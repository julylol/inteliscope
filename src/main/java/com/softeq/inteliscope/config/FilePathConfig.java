package com.softeq.inteliscope.config;

public class FilePathConfig {
    private static final String KEY_INTELISCOPE_PATH_MERGE = "inteliscope_path_merge";
    private static final String KEY_INTELISCOPE_PATH_PROMO = "inteliscope_path_promo";
    private static final String KEY_INTELISCOPE_PATH_UPLOAD = "inteliscope_path_upload";
    private static final String KEY_INTELISCOPE_PATH_ZIP = "inteliscope_path_zip";
    private static final String KEY_INTELISCOPE_PATH_MERGE_UTIL = "inteliscope_path_merge_util";

    public static String getPathMerge() {
        return System.getProperty(KEY_INTELISCOPE_PATH_MERGE);
    }

    public static String getPathPromo() {
        return System.getProperty(KEY_INTELISCOPE_PATH_PROMO);
    }

    public static String getPathUpload() {
        return System.getProperty(KEY_INTELISCOPE_PATH_UPLOAD);
    }

    public static String getPathZip() {
        return System.getProperty(KEY_INTELISCOPE_PATH_ZIP);
    }

    public static String getPathMergeUtil() {
        return System.getProperty(KEY_INTELISCOPE_PATH_MERGE_UTIL);
    }
}
