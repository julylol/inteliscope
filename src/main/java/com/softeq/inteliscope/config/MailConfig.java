package com.softeq.inteliscope.config;

public class MailConfig {
    private static final String KEY_INTELISCOPE_MAIL_CONFIRM_UPLOAD_BODY = "inteliscope_mail_confirm_upload_body";
    private static final String KEY_INTELISCOPE_MAIL_CONFIRM_UPLOAD_SUBJECT = "inteliscope_mail_confirm_upload_subject";
    private static final String KEY_INTELISCOPE_MAIL_SMTP_USERNAME = "inteliscope_mail_smtp_username";
    private static final String KEY_INTELISCOPE_MAIL_SMTP_PASSWORD = "inteliscope_mail_smtp_password";
    private static final String KEY_INTELISCOPE_MAIL_SMTP_PORT = "inteliscope_mail_smtp_port";
    private static final String KEY_INTELISCOPE_MAIL_SMTP_HOST = "inteliscope_mail_smtp_host";

    public static String getMailConfirmUploadBody() {
        return System.getProperty(KEY_INTELISCOPE_MAIL_CONFIRM_UPLOAD_BODY);
    }

    public static String getMailConfirmUploadSubject() {
        return System.getProperty(KEY_INTELISCOPE_MAIL_CONFIRM_UPLOAD_SUBJECT);
    }

    public static String getMailSmtpUsername() {
        return System.getProperty(KEY_INTELISCOPE_MAIL_SMTP_USERNAME);
    }

    public static String getMailSmtpPassword() {
        return System.getProperty(KEY_INTELISCOPE_MAIL_SMTP_PASSWORD);
    }

    public static String getMailSmtpPort() {
        return System.getProperty(KEY_INTELISCOPE_MAIL_SMTP_PORT);
    }

    public static String getMailSmtpHost() {
        return System.getProperty(KEY_INTELISCOPE_MAIL_SMTP_HOST);
    }
}
