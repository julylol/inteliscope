package com.softeq.inteliscope.config;

public class YoutubeConfig {
    private static final String KEY_INTELISCOPE_YOUTUBE_CLIENT_SECRETS = "inteliscope_youtube_client_secrets";
    private static final String KEY_INTELISCOPE_YOUTUBE_REFRESH_TOKEN = "inteliscope_youtube_refresh_token";

    public static String getYoutubeClientSecrets() {
        return System.getProperty(KEY_INTELISCOPE_YOUTUBE_CLIENT_SECRETS);
    }

    public static String getYoutubeRefreshToken() {
        return System.getProperty(KEY_INTELISCOPE_YOUTUBE_REFRESH_TOKEN);
    }
}
