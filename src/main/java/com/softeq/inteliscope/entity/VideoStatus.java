package com.softeq.inteliscope.entity;

public enum VideoStatus {
	
	NEW("new"),
	MERGED("merged"),
	MERGEFAILED("mergefailed"),
	COMPLETED("completed");
	
	private String status;
	
	VideoStatus(String status) {
		this.status = status;
	}
	
	public String getStatus() {
		return status;
	}
	
}
