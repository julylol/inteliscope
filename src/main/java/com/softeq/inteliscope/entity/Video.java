package com.softeq.inteliscope.entity;

public class Video {

	private Integer id;
	private String filename;
	private String status;	
	private String email;
	private String url;
	private String title;
	private String description;	
	private String tags;
	private String height;
	private String width;
	private String category;
	private String mergeDetails;

	private Boolean ageRestriction;		

	public Video() {
		// TODO Auto-generated constructor stub
	}
	
	public Video(Integer id, String videoTitle, String videoDescription,String videoTags,String height,String width,String category, String filename, String status, String email, String url, String mergeDetails, Boolean ageRestriction) {
		this.id = id;
		this.title = videoTitle;
		this.description = videoDescription;
		this.tags = videoTags;
		this.height = width;
		this.category = category;
		this.filename = filename;
		this.status = status;
		this.email = email;
		this.url = url;	
		this.mergeDetails = mergeDetails;
		this.ageRestriction = ageRestriction;		
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String videoTitle) {
		this.title = videoTitle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String videoDescription) {
		this.description = videoDescription;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String videoTags) {
		this.tags = videoTags;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}	

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getMergeDetails() {
		return mergeDetails;
	}

	public void setMergeDetails(String mergeDetails) {
		this.mergeDetails = mergeDetails;
	}
			
	public Boolean getAgeRestriction() {
		return ageRestriction;
	}

	public void setAgeRestriction(Boolean ageRestriction) {
		this.ageRestriction = ageRestriction;
	}
}
