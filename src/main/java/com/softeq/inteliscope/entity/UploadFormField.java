package com.softeq.inteliscope.entity;

public enum UploadFormField {
	TITLE("title"),
	DESCRIPTION("description"),
	TAGS("tags"),
	CATEGORY("category"),
	EMAIL("email"),
	WIDTH("width"),
	HEIGHT("height"),
	FILE("file"),
	FILEMETADATA("metadata"),
	AGERESTRICTION("ageRestriction"),
	CHANNELDEFAULT("rs_channel_default");
	
	private String name;
	
	UploadFormField(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}