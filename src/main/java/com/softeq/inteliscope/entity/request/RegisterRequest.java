package com.softeq.inteliscope.entity.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterRequest {
    @JsonProperty("email")
    @NotNull
    @NotEmpty
    private String email;
    @JsonProperty("password")
    @NotNull
    @NotEmpty
    private String password;
    @JsonProperty("name")
    @NotNull
    @NotEmpty
    private String name;
    @JsonProperty("product")
    @NotNull
    @NotEmpty
    private String product;
    @JsonProperty("shooter_type")
    @NotNull
    @NotEmpty
    private String shooterType;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getShooterType() {
        return shooterType;
    }

    public void setShooterType(String shooterType) {
        this.shooterType = shooterType;
    }
}
