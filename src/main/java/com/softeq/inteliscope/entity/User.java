
package com.softeq.inteliscope.entity;

import com.softeq.inteliscope.entity.request.RegisterRequest;

public class User {
	private String email;
	private String password;
	private String name;
	private String product;
	private String shooterType;
	private String authToken;
	private String registrationToken;
	private Boolean isEnabled;

	public User(){
	}

	public User(RegisterRequest request, String authToken, String registrationToken) {
		this.email = request.getEmail();
		this.password = request.getPassword();
		this.name = request.getName();
		this.product = request.getProduct();
		this.shooterType = request.getShooterType();
		this.authToken = authToken;
		this.registrationToken = registrationToken;
		this.isEnabled = false;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getShooterType() {
		return shooterType;
	}

	public void setShooterType(String shooterType) {
		this.shooterType = shooterType;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public Boolean isEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(Boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	@Override
	public String toString() {
		return "User{" +
				"email='" + email + '\'' +
				", password='" + password + '\'' +
				", name='" + name + '\'' +
				", product='" + product + '\'' +
				", shooterType='" + shooterType + '\'' +
				", authToken='" + authToken + '\'' +
				", isEnabled=" + isEnabled +
				'}';
	}
}
