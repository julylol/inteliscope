package com.softeq.inteliscope.facebook;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;

public class FacebookClient {

	private static final String FACEBOOK_TOKEN_URL = "https://graph.facebook.com/me?access_token=";

	/**
	 * extract user email by Facebook token
	 */
	public String getFacebookEmailByToken(String token) {

		CloseableHttpClient client = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(FACEBOOK_TOKEN_URL + token);

		try {

			HttpResponse response = client.execute(httpGet);
			if (!verifyStatusCode(response.getStatusLine().getStatusCode()))
				return null;

			return getDecodedFacebookEmail(getResponseContent(response));
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 * decode special email characters
	 * @throws UnsupportedEncodingException
	 */
	private String getDecodedFacebookEmail(String responseContent) throws IOException {

		String email = getFacebookEmail(responseContent);

		if (email == null || email.isEmpty()) return null;

		if (email.contains("\u0040")) {
			email = email.replace("\u0040", "@");
		}

		if (email.contains("%")) {
			email = URLDecoder.decode(email, "UTF-8");
		}

		return email;
	}

	/**
	 * retrieve user email from the Facebook response API
	 */
	private String getFacebookEmail(String responseContent) throws IOException {
		if (null == responseContent || responseContent.isEmpty()) return null;

		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> object = mapper.readValue(responseContent, new TypeReference(){});
		return (String)object.get("mail");
	}

	/**
	 * get Facebook user data
	 */
	private String getResponseContent(HttpResponse response) throws IOException {

			StringBuilder result = new StringBuilder();

			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));

			String line;
			while ((line = bufferedReader.readLine()) != null) {
				result.append(line);
			}

			bufferedReader.close();

			return result.toString();
	}

	/**
	 * verify response code is success
	 */
	private boolean verifyStatusCode(int statusCode) {
		return statusCode < 400;
	}

}
