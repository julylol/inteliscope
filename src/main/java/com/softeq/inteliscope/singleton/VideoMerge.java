package com.softeq.inteliscope.singleton;

import com.softeq.inteliscope.config.FilePathConfig;
import com.softeq.inteliscope.dao.VideoDao;
import com.softeq.inteliscope.entity.Video;
import com.softeq.inteliscope.entity.VideoStatus;

import org.jboss.logging.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.inject.Inject;

public class VideoMerge {

	private static final String APPLICATION_NAME = "RIFLESCOPE";

	private static final String MSG_FILE_DELETE_FAILED = APPLICATION_NAME
			+ " : " + "FAILED TO DELETE ORIGINAL VIDEO FILE: %s";

	private static final String MSG_MERGE_FAILED = APPLICATION_NAME + " : "
			+ "FAILED TO MERGE VIDEO #%s (%s)";

	private static final String MSG_MERGE_SUCCESS = APPLICATION_NAME + " : "
			+ "MERGING VIDEO PERFORMED SUCCESSFULLY #%s (%s)";

	private static final String MSG_MERGE_STARTED = APPLICATION_NAME + " : "
			+ "MERGING VIDEO STARTED #%s (%s)";
	
	private volatile static VideoMerge instance;
	private static final Logger LOGGER = Logger.getLogger(VideoMerge.class);

	@Inject
	private VideoDao videoDao;

	private VideoMerge() {
	}

	public static VideoMerge getInstance() {

		VideoMerge localInstance = instance;
		if (null == localInstance) {

			if (localInstance == null) {
				synchronized (VideoMerge.class) {
					localInstance = instance;
					if (localInstance == null) {
						instance = localInstance = new VideoMerge();
					}
				}
			}
			return localInstance;
		}

		return localInstance;
	}

	public void runVideoMerge(Video video) {

	        LOGGER.info(String.format(MSG_MERGE_STARTED, video.getId(), video.getTitle()));

	        try {
	            LOGGER.info("START MERGE PROCESS...");
	            
	            final Process mergeProcess = startMergeProcess(video);
	            //External process is running and it's writing a lot of crap in to buffer

	            //Bugger size is limited! So we need to read from buffer to empty it  
	            final BufferedReader r = new BufferedReader(new InputStreamReader(mergeProcess.getInputStream()));
	            final StringBuffer output = new StringBuffer();
	            
	            String line;
	            while ((line = r.readLine()) != null) {
	            	LOGGER.info(line);
	                output.append(line);
	            }

	            //External process done, and we get exit code
	            final int execStatus = mergeProcess.waitFor();

	            LOGGER.info("AFTER MERGE PROCESS...");
	            LOGGER.info("execStatus"+" "+execStatus);
	            
	            boolean mergeStatus = execStatus == 0;
	            LOGGER.info("mergeStatus : " + mergeStatus);
	            video.setMergeDetails(output.toString());

	            videoDao.setMerged(video, mergeStatus);

	            if (mergeStatus) {
	                LOGGER.info(String.format(MSG_MERGE_SUCCESS, video.getId(),
	                        video.getTitle()));

	                removeSourceFile(video);

	            } else {
	                LOGGER.error(String.format(MSG_MERGE_FAILED, video.getId(),
	                        video.getTitle()));
	            }

	        } catch (IOException | InterruptedException e) {
	            videoDao.updateStatus(video, VideoStatus.MERGEFAILED.getStatus());

	            LOGGER.error(String.format(MSG_MERGE_FAILED, video.getId(),
	                    video.getTitle()));
	            LOGGER.error(e);
	        } 
	    }

	private void removeSourceFile(Video video) {

		try {
			Path sourceFilePath = Paths.get(getSourceFilePath(video));
			Files.delete(sourceFilePath);
		} catch (IOException | IllegalArgumentException e) {
			LOGGER.error(String.format(MSG_FILE_DELETE_FAILED,
					video.getFilename()));
			LOGGER.error(e);
		}
	}
	
	private Process startMergeProcess(Video video) throws IOException {

		List<String> params = java.util.Arrays.asList("sh",
					getMergeUtilPath(),
					video.getFilename());

		    File dir = new File("/opt/riflescope/");
			ProcessBuilder pb = new ProcessBuilder(params);
			pb.directory(dir);
			pb.redirectErrorStream(true);
			return pb.start();
	}			

	/**
	 * path to the merging utility script file
	 */
	private String getMergeUtilPath() {
		return FilePathConfig.getPathMergeUtil();

	}

	/**
	 * path to the incoming video file
	 */
	private String getSourceFilePath(Video video) {
		return FilePathConfig.getPathUpload() + video.getFilename();
	}


}
