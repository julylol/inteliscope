package com.softeq.inteliscope.singleton;

import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.http.InputStreamContent;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.VideoSnippet;

import com.softeq.inteliscope.config.FilePathConfig;
import com.softeq.inteliscope.entity.Video;

import org.jboss.logging.Logger;
import org.springframework.util.StringUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;

//import com.google.api.services.youtube.model.VideoAgeGating;

/**
 * upload videos to YouTube
 * YouTube accepts only 2 simultaneous uploads, we're limiting our application to a single thread
 */
public class YoutubeUpload {
	
	private volatile static YoutubeUpload instance;
	private static final Logger LOGGER = Logger.getLogger(YoutubeUpload.class);
	
	private YoutubeUpload(){
	}
	
	/**
	 * singleton
	 */
	public static YoutubeUpload getInstance() {

		YoutubeUpload localInstance = instance;
		if (null == localInstance) {
			
	        if (localInstance == null) {
	            synchronized (YoutubeUpload.class) {
	                localInstance = instance;
	                if (localInstance == null) {
	                    instance = localInstance = new YoutubeUpload();
	                }
	            }
	        }
	        return localInstance;
		}
		
		return localInstance;
	}
	
	/**
	 * upload video by the pre-configured YouTube service
	 * @throws IOException
	 */
	public com.google.api.services.youtube.model.Video uploadVideo(Video video,
			YouTube youtube) throws IOException {
		
		final String VIDEO_FILE_FORMAT = "video/*";

		File videoFile = getUploadFile(video);
			
		com.google.api.services.youtube.model.Video videoMetadata = initYouTubeVideo(video);

		InputStreamContent mediaContent = new InputStreamContent(
				VIDEO_FILE_FORMAT, new BufferedInputStream(new FileInputStream(
						videoFile)));
		mediaContent.setLength(videoFile.length());

		YouTube.Videos.Insert videoInsert = youtube.videos().insert(
				"snippet,statistics,status", videoMetadata, mediaContent);

		MediaHttpUploader uploader = videoInsert.getMediaHttpUploader();
		uploader.setDirectUploadEnabled(false);

		return videoInsert.execute();
	}

	/**
	 * create YouTube video object and populate with metadata
	 */
	private com.google.api.services.youtube.model.Video initYouTubeVideo(Video video) {
		
		VideoSnippet snippet = new VideoSnippet();

		snippet.setTitle(video.getTitle());
		snippet.setDescription(video.getDescription());
		snippet.setCategoryId(video.getCategory());

		String videoTags = video.getTags();
		if (null != videoTags && !videoTags.isEmpty()) {
			if (videoTags.contains(",")) {
				snippet.setTags(Arrays.asList(StringUtils.split(video.getTags(), ",")));
			} else {
				snippet.setTags(Arrays.asList(videoTags));
			}
		}

		com.google.api.services.youtube.model.Video videoMetadata = new com.google.api.services.youtube.model.Video();
		videoMetadata.setSnippet(snippet);
		
		// by now youtube API prohibits specifying AgeGating, Response = FORBIDDEN
//		if(video.getAgeRestriction()) {
//			VideoAgeGating gating = new VideoAgeGating();
//			gating.setRestricted(true);
//			videoMetadata.setAgeGating(gating);	
//		}
		
		return videoMetadata;
	}

	/**
	 * get video file location
	 */
	private File getUploadFile(Video video) {
		String filePath = FilePathConfig.getPathMerge() + video.getFilename();
		return new File(filePath);
	}
	

}
