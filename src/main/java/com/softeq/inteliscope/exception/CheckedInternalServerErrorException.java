package com.softeq.inteliscope.exception;

import javax.ejb.ApplicationException;
import javax.ws.rs.InternalServerErrorException;

@ApplicationException
public class
        CheckedInternalServerErrorException extends InternalServerErrorException {
    public CheckedInternalServerErrorException() {
    }

    public CheckedInternalServerErrorException(String message) {
        super(message);
    }

    public CheckedInternalServerErrorException(Throwable cause) {
        super(cause);
    }
}
