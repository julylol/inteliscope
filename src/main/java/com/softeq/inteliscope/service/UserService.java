package com.softeq.inteliscope.service;

import com.softeq.inteliscope.entity.request.RegisterRequest;
import com.softeq.inteliscope.entity.response.RegisterResponse;

public interface UserService {
    RegisterResponse register(RegisterRequest registerRequest);
}
