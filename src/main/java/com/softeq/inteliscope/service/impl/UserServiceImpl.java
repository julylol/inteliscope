package com.softeq.inteliscope.service.impl;

import com.softeq.inteliscope.config.UserConfig;
import com.softeq.inteliscope.dao.UserDao;
import com.softeq.inteliscope.entity.User;
import com.softeq.inteliscope.entity.request.RegisterRequest;
import com.softeq.inteliscope.entity.response.RegisterResponse;
import com.softeq.inteliscope.service.UserService;
import com.softeq.inteliscope.util.DataGenerator;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class UserServiceImpl implements UserService {
    @Inject
    private UserDao userDao;

    @Inject
    private DataGenerator dataGenerator;

    @Override
    public RegisterResponse register(RegisterRequest registerRequest) {
        String authToken = dataGenerator.getAuthToken(registerRequest.getEmail(), registerRequest.getPassword());
        String registrationToken = dataGenerator.generateRandomString(UserConfig.getConfirmTokenLength());
        User user = new User(registerRequest, authToken, registrationToken);
        userDao.insert(user);
        return new RegisterResponse(authToken);
    }
}
