package com.softeq.inteliscope.service.impl;

import com.softeq.inteliscope.config.MailConfig;
import com.softeq.inteliscope.service.EmailService;

import org.jboss.logging.Logger;

import java.util.Date;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Stateless
public class EmailServiceImpl implements EmailService {

    private static final Logger LOGGER = Logger.getLogger(EmailServiceImpl.class);

    public void sendEmail(final String to, final String subject, final String msg) {

        Properties props = new Properties();
        props.put("mail.smtp.host", MailConfig.getMailSmtpHost());
        props.put("mail.smtp.port", MailConfig.getMailSmtpPort());
        props.put("mail.smtp.auth", true);
        props.put("mail.smtp.starttls.enable", true);

        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(MailConfig.getMailSmtpUsername(), MailConfig.getMailSmtpPassword());
            }
        };
        Session session = Session.getInstance(props, auth);

        sendEmail(session, to, subject, msg);
    }

    public static void sendEmail(Session session, String toEmail, String subject, String body) {
        MimeMessage msg = new MimeMessage(session);
        try {
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.addHeader("format", "flowed");
            msg.addHeader("Content-Transfer-Encoding", "8bit");

            msg.setSubject(subject, "UTF-8");

            msg.setContent(body, "text/HTML; charset=utf-8");

            msg.setSentDate(new Date());

            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
            Transport.send(msg);
        } catch (MessagingException e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }
}
