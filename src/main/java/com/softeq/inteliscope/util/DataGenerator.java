package com.softeq.inteliscope.util;

import com.softeq.inteliscope.config.UserConfig;
import com.softeq.inteliscope.exception.CheckedInternalServerErrorException;

import org.apache.commons.lang.RandomStringUtils;
import org.jboss.logging.Logger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.ejb.Stateless;

@Stateless
public class DataGenerator {

    private static final Logger LOGGER = Logger.getLogger(DataGenerator.class);

    public String getAuthToken(String email, String password) {
        return getSecureString((new StringBuilder(email).append(":").append(password)).toString());
    }

    public String getSecureString(String str) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(UserConfig.getUserAuthTokenSalt().getBytes());
            byte[] bytes = md.digest(str.getBytes());
            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();

        } catch (NoSuchAlgorithmException e) {
            LOGGER.error(e.getMessage(), e);
            throw new CheckedInternalServerErrorException();
        }
    }

    public static String generateRandomString(int length) {
        return RandomStringUtils.randomAlphanumeric(length);
    }
}
