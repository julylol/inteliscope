package com.softeq.inteliscope.spring;

import com.softeq.inteliscope.dao.UserDao;
import com.softeq.inteliscope.dao.VideoDao;
import com.softeq.inteliscope.dao.impl.UserDaoImpl;
import com.softeq.inteliscope.dao.impl.VideoDaoImpl;
import com.softeq.inteliscope.service.UserService;
import com.softeq.inteliscope.service.impl.UserServiceImpl;
import com.softeq.inteliscope.util.DataGenerator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@Configuration
@ComponentScan({ "com.softeq.inteliscope" })
@EnableTransactionManagement
public class ApplicationContextConfig {


    @Bean
    @Autowired
    public VideoDao videoDao() {
        return new VideoDaoImpl();
    }

    @Bean
    @Autowired
    public UserDao userDao() {
        return new UserDaoImpl();
    }

    @Bean
    @Autowired
    public UserService userService() {
        return new UserServiceImpl();
    }

    @Bean
    @Autowired
    public DataGenerator dataConverter() {
        return new DataGenerator();
    }
}
